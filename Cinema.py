import threading


class Cinema:
    def __init__(self, room_cinema_list):
        self.roomCinemaList = room_cinema_list

    def all_finished(self):
        for room in self.roomCinemaList:
            if not room.finished:
                return False
        return True

    def simulateCinema(self):
        threads = []
        for room in self.roomCinemaList:
            thread = threading.Thread(target=room.simulate_room)
            threads.append(thread)

        # Lance tous les threads
        for thread in threads:
            thread.start()

        # Attend la fin de tous les threads
        for thread in threads:
            thread.join()
