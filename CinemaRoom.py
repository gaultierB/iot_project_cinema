import json
import time
import random

from Co2 import Co2
from Movie import Movie
from Temperature import Temperature


class CinemaRoom:
    def __init__(self, name_room, mqtt_client, movie):
        self.number_of_people = 0
        self.limit_room = 200
        self.name_room = name_room
        self.temperature = Temperature(self)
        self.mqtt_client = mqtt_client
        self.movie = movie
        self.co2 = Co2(self)
        self.time_leave = 60  # time in second
        self.finished = False

    def increment_people(self, number_of_people_entered):
        self.number_of_people += number_of_people_entered

    def decrement_people(self, number_of_people_leave):
        if self.number_of_people - number_of_people_leave > 0:
            self.number_of_people -= number_of_people_leave
        else:
            print("No negative value nobody leaved")

    def set_temperature(self, temperature):
        self.temperature = temperature

    def get_number_of_people(self):
        return self.number_of_people

    def get_temperature(self):  
        return self.temperature

    def simulate_person_enter(self):
        number_of_people_entered = random.randint(1, 10)
        if self.number_of_people + number_of_people_entered < self.limit_room:
            self.increment_people(number_of_people_entered)
        else:
            print("limit person")

    def simulate_person_leave(self):
        number_of_people_leave = random.randint(1, 10)
        self.decrement_people(number_of_people_leave)

    def simulate_room(self):
        print("Start simulating room")
        count_time = 0
        while self.movie.duration_of_movie > count_time:

            # TEMPERATURE
            print("simulate temperature")
            self.temperature.simulate_temperature()
            self.mqtt_client.publish_message(self.temperature.nameTopic, self.temperature.value)
            self.mqtt_client.listen_topic(self.name_room + "/clim")
            # CO2
            print("simulate CO2")
            self.co2.simulate_co2_level()
            self.mqtt_client.publish_message(self.co2.nameTopic, self.co2.co2_level)

            # PEOPLE
            print("simulate person enter")
            self.simulate_person_enter()
            print("Room : " + self.name_room + " ENTER " + str(self.number_of_people))
            self.mqtt_client.publish_message(self.name_room + "/number_of_people", self.get_number_of_people())
            time.sleep(5)
            count_time += 5

        # MOVIE FINISHED PEOPLE LEAVE
        count_time = 0
        while self.time_leave > count_time:
            print("start leave")
            # PEOPLE
            self.simulate_person_leave()
            self.mqtt_client.publish_message(self.name_room + "/number_of_people", self.get_number_of_people())
            time.sleep(10)
            count_time += 10
        print("Room : " + self.name_room + " finished")
        self.finished = True

    def to_json(self):
        return json.dumps({
            "number_of_people": self.number_of_people,
            "limit_room": self.limit_room,
            "name_room": self.name_room,
            "temperature": self.temperature.to_json(),
            "movie": self.movie.to_json(),
            "co2": self.co2.to_json(),
            "time_leave": self.time_leave
        })