import json
import random


class Co2:
    def __init__(self, cinemaRoom):
        self.co2_level = 1000
        self.co2_ratio = 0.01
        self.cinemaRoom = cinemaRoom
        self.nameTopic = cinemaRoom.name_room + "/CO2"

    def simulate_co2_level(self):
        co2_variation = random.uniform(-0.005, 0.005)  # Variation aléatoire de -0.005 à 0.005
        self.co2_level = round(self.co2_level + (self.cinemaRoom.get_number_of_people() * self.co2_ratio) + co2_variation, 2)

    def get_co2_level(self):
        return self.co2_level

    def to_json(self):
        return json.dumps({
            "co2_level": self.co2_level,
            "nameTopic": self.nameTopic
        })
