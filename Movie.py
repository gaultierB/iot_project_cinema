import json


class Movie:

    def __init__(self, name_movie, duration_of_movie):
        self.name_movie = name_movie
        self.duration_of_movie = duration_of_movie

    def to_json(self):
        return json.dumps({
            "name_movie": self.name_movie,
            "duration_of_movie": self.duration_of_movie
        })