# Cinema Room Simulation

Ce projet est une simulation de salle de cinéma qui surveille la température, le niveau de CO2 et le nombre de personnes dans la salle. Il utilise le protocole MQTT pour publier les données à un serveur MQTT.

## Installation

Avant de commencer, assurez-vous d'avoir Python et pip installés sur votre système.

1. Clonez ce dépôt GitHub sur votre machine locale :

   ```shell
   git clone https://github.com/votre-utilisateur/cinema-room-simulation.git

Accédez au répertoire du projet :

   cd cinema-room-simulation

## Installez les dépendances requises à l'aide de pip :

    pip install -r requirements.txt

Démarrage de la simulation
Pour lancer la simulation de la salle de cinéma, exécutez la commande suivante :

   python main.py
   
La simulation générera des données de température, de CO2 et de nombre de personnes dans la salle en publiant les messages MQTT correspondants. Les données peuvent être visualisées à l'aide d'un client MQTT approprié.
