import json
import random
import time


class Temperature:
    def __init__(self, cinemaRoom, value=0):
        self.value = value
        self.cinemaRoom = cinemaRoom
        self.nameTopic = self.cinemaRoom.name_room + "/" + "temperature"
        self.base_temperature = 20.0  # Température de base de la salle
        self.temperature_per_person = 0.1  # Variation de température par personne

    def set_value(self, value):
        self.value = value

    def get_value(self):
        return self.value

    def simulate_temperature(self):
        random_factor = random.uniform(-0.5, 0.5)  # Facteur aléatoire entre -0.5 et 0.5
        print(self.cinemaRoom.mqtt_client.value_clim)
        if self.cinemaRoom.mqtt_client.value_clim[self.cinemaRoom.name_room + "/clim"] == "on":
            print("on clim")
            reduction_factor = self.cinemaRoom.get_number_of_people() * self.temperature_per_person * 0.1
            if reduction_factor > 0:
                simulated_temperature = self.value - reduction_factor + random_factor
            else:
                simulated_temperature = self.value + random_factor
        else:
            simulated_temperature = self.base_temperature + (
                    self.cinemaRoom.get_number_of_people() * self.temperature_per_person) + random_factor

        min_temperature = 15
        max_temperature = 38.0
        simulated_temperature = max(min(simulated_temperature, max_temperature), min_temperature)

        self.set_value(round(simulated_temperature, 2))

    def to_json(self):
        return json.dumps({
            "value": self.value,
            "nameTopic": self.nameTopic
        })
