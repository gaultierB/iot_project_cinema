from time import sleep

import Cinema
import Movie
import CinemaRoom
import process_mqtt

if __name__ == '__main__':
    while True:
        client = process_mqtt.MqttClient()
        movie1 = Movie.Movie("Intouchables", 200)
        cinema_room1 = CinemaRoom.CinemaRoom("room_1", client, movie1)
        cinema_room2 = CinemaRoom.CinemaRoom("room_2", client, movie1)
        cinema_room3 = CinemaRoom.CinemaRoom("room_3", client, movie1)
        cinema = Cinema.Cinema([cinema_room1, cinema_room2, cinema_room3])
        cinema.simulateCinema()