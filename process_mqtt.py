import paho.mqtt.client as paho
from paho import mqtt
import Temperature
import time
import CinemaRoom


class MqttClient:
    def __init__(self, broker="3dbd3ae102d847199a686471afce2949.s1.eu.hivemq.cloud", port=8883, username="gogoB", password="GvAdtQFVs.8bk2N"):
        self.broker = broker
        self.port = port
        self.value_clim = {"room_1/clim": "off", "room_2/clim": "off", "room_3/clim": "off"}
        self.username = username  # Nom d'utilisateur MQTT
        self.password = password  # Mot de passe MQTT
        self.client = paho.Client(client_id="", userdata=None, protocol=paho.MQTTv5)
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_message = self.on_message
        self.message_callback = None
        self.client.tls_set(tls_version=mqtt.client.ssl.PROTOCOL_TLS)
        self.client.username_pw_set(username, password) # Définition du nom d'utilisateur et du mot de passe
        self.connect()

    def connect(self):
        self.client.connect(self.broker, self.port)
        self.client.loop_start()

    def disconnect(self):
        self.client.loop_stop()
        self.client.disconnect()

    def on_disconnect(self, client, userdata, rc):
        if rc != 0:
            print("Unexpected disconnection from MQTT broker")

    def on_connect(self, client, userdata, flags, rc, properties=None):
        if rc == 0:
            print("Connexion établie avec succès !")
        else:
            print("Échec de la connexion. Code de retour =", rc)

    def publish_message(self, topic, message):
        self.client.publish(topic, message)

    def listen_topic(self, topic):
        self.client.subscribe(topic)

    def on_message(self, client, userdata, msg):
        self.value_clim[msg.topic] = msg.payload.decode()
